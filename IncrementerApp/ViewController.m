//
//  ViewController.m
//  IncrementerApp
//
//  Created by James Cash on 16-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "NumberModel.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *firstNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdNumberLabel;

@property (nonatomic,strong) NumberModel *model;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.model = [[NumberModel alloc] init];
//    NumberModel *model = [[NumberModel alloc] init];
    [self updateNumberDisplay];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNumberDisplay
{
    self.firstNumberLabel.text = [NSString stringWithFormat:@"%ld", self.model.firstNumber];
    self.secondNumberLabel.text = [NSString stringWithFormat:@"%ld", self.model.secondNumber];
    self.thirdNumberLabel.text = [NSString stringWithFormat:@"%ld", self.model.thirdNumber];
}

- (IBAction)numberDown:(UIButton*)sender {
    NSLog(@"Number %ld down", (long)sender.tag);
    switch (sender.tag) {
        case 1:
            [self.model decerementFirst];
            break;
        case 2:
            [self.model decerementSecond];
            break;
        case 3:
            [self.model decerementThird];
            break;
        default:
            break;
    }
    [self updateNumberDisplay];
}

- (IBAction)numberUp:(UIButton*)sender {
    NSLog(@"Number %ld up", (long)sender.tag);
    switch (sender.tag) {
        case 1:
            [self.model incrementFirst];
            break;
        case 2:
            [self.model incrementSecond];
            break;
        case 3:
            [self.model incrementThird];
            break;
        default:
            break;
    }
    [self updateNumberDisplay];
}


@end
