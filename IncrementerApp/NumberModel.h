//
//  NumberModel.h
//  IncrementerApp
//
//  Created by James Cash on 16-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NumberModel : NSObject

@property (nonatomic, readonly, assign) NSInteger firstNumber;
@property (nonatomic, readonly, assign) NSInteger secondNumber;
@property (nonatomic, readonly, assign) NSInteger thirdNumber;

- (void)incrementFirst;
- (void)decerementFirst;

- (void)incrementSecond;
- (void)decerementSecond;

- (void)incrementThird;
- (void)decerementThird;
@end
