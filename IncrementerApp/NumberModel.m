//
//  NumberModel.m
//  IncrementerApp
//
//  Created by James Cash on 16-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "NumberModel.h"

@implementation NumberModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _firstNumber = 1;
        _secondNumber = 2;
        _thirdNumber = 3;
    }
    return self;
}

- (void)incrementFirst
{
    _firstNumber++;
}

- (void)decerementFirst
{
    if (self.firstNumber <= 0) { return; }
    _firstNumber--;
}

- (void)incrementSecond
{
    if (self.secondNumber >= 5) { return; }
    _secondNumber++;
}

-(void)decerementSecond
{
    _secondNumber--;
}

- (void)incrementThird
{
    _thirdNumber++;
}

- (void)decerementThird
{
    _thirdNumber--;
}

@end
